<div class=" bottom-0 w-full bg-blue-800">
    <div class="flex w-full text-white xl:container ">
        <div class="w-full mx-6 lg:mx-0 lg:px-36">
            <div class="md:flex">
                <div class="self-center w-24 py-5 md:ml-10 sm:w-28 xl:ml-32 md:justify-between "><a href="/"><img
                            src="https://medical-desk-it.web.app/images/logo.png" alt="logo"></a>
                    <p class="flex mt-8">
                        Addressa: Tirane
                        Email: online@example.com
                        Phone: xxx-xxx-xxx
                    </p>
                </div>
                <div>
                    <h3 class="text-bold text-2xl font-bold py-5  md:ml-44 sm:ml-24">Support</h3>
                    <p class="md:ml-44 sm:ml-24 sm:grid grid-rowa-3">
                        Faq &nbsp;&nbsp;
                        Privacy &nbsp;&nbsp;
                        Termini e condizicioni
                    </p>
                </div>


            </div>
            <div>

                <p class="md:ml-10 xl:ml-32 mt-5 mb-12 text-xs  lg:text-sm">Medical Desk by LEO NARDI srl – Tel +39 02
                    82196941
                    – Sede Legale: Viale Vittorio Veneto, 20 – 20124 – Milano – Italy P.Iva 0843666096</p>
            </div>
        </div>
    </div>
</div>
