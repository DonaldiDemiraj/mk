<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <script>
        function showStuff(id, text, btn) {
            document.getElementById(id).style.display = 'block';
        }

        function showStuff1(id, text, btn) {
            document.getElementById(id).style.display = 'block';
        }

        function showStuff3(id, text, btn) {
            document.getElementById(id).style.display = 'block';
        }

        function show4(id, text, btn) {
            document.getElementById(id).style.display = 'block';
        }

        function myFunction() {
            document.getElementById("out1").style.background = "#16c5b3";
            document.getElementById("text3").style.background = "#16c5b3";

        }

        function fun(event) {
            console.log(event);
            const {
                path
            } = event;
            for (el of path) {
                if (el.id === 'description') {
                    const {
                        children
                    } = el;

                    for (childEl of children) {
                        //console.log(children);
                        childEl.classList.remove('button-clicked');
                    }
                }
            }
            const {
                nodeName,
                classList
            } = event.target;
            if (nodeName === 'BUTTON') {
                classList.add("button-clicked");
            }

        }

        function sub(event) {
            console.log(event);
            const {
                submitter
            } = event;
            if (submitter.nodeName === 'BUTTON' && submitter.type !== 'submit') {
                event.preventDefault();
            }
        }

        $(document).ready(function() {
            $('#go').click(function() {
                $("#chekout-date").val('1');
            })
        });

        // document.getElementById('text4').onclick = function() {
        //     document.getElementById("1").value = document.getElementById("answer4").value;
        // }
    </script>
    <style>
        .button-clicked {
            background-color: rgb(12, 148, 148) !important;
            color: rgb(255, 255, 255);
        }

    </style>
</head>

<body>
    @include('components.header')

    {{-- @include('components.tamponerapido') --}}

    <div class="block mx-8 mb-24 lg:mb-32 lg:flex lg:mx-auto sm:px-16 mt-40 xl:container">
        <div class="w-8 h-8 mt-8 lg:mt-10">
            <a href="/"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0)">
                        <path
                            d="M15.0017 30C23.2726 30 30.0014 23.2709 30.0014 14.9999C30.0014 6.72906 23.2726 0 15.0017 0C6.73087 0 0.00195312 6.72906 0.00195312 14.9999C0.00195312 23.2709 6.73087 30 15.0017 30ZM15.0017 2.05338C22.1404 2.05338 27.948 7.86116 27.9482 14.9999C27.9482 22.1387 22.1405 27.9465 15.0017 27.9466C7.86311 27.9465 2.05547 22.1387 2.05547 14.9998C2.05547 7.86129 7.86311 2.05338 15.0017 2.05338Z"
                            fill="#09425A"></path>
                        <path
                            d="M12.9859 21.4246C13.3868 21.8254 14.0369 21.8252 14.4377 21.4246C14.8388 21.0235 14.8388 20.3735 14.4376 19.9724L10.4924 16.0273L21.917 16.0262C22.484 16.0261 22.9435 15.5665 22.9435 14.9993C22.9434 14.4323 22.4838 13.9728 21.9168 13.9728L10.4918 13.9739L14.438 10.028C14.839 9.62708 14.839 8.97684 14.438 8.57602C14.2375 8.37561 13.9748 8.27527 13.7119 8.27527C13.4492 8.27527 13.1865 8.37561 12.986 8.57588L7.28732 14.2744C7.09472 14.4669 6.98657 14.7279 6.98657 15.0004C6.98671 15.2728 7.09485 15.5337 7.28746 15.7266L12.9859 21.4246Z"
                            fill="#09425A"></path>
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="30" height="30" fill="white"></rect>
                        </clipPath>
                    </defs>
                </svg>
            </a>
        </div>
        <div class="w-full sm:px-20">
            <h2 class="mt-6 text-2xl font-bold text-center uppercase text-blue-900 lg:text-left lg:text-3xl lg:mt-10">
                PRENOTA
                IL TUO ESAME</h2>
            <p class="mt-4 mb-10 text-sm text-center text-primary lg:text-base lg:text-left lg:mt-10 lg:mb-12">
                All’interno
                delle strutture MedicalDesk e dei mezzi è necessario indossare la mascherina e rispettare la distanza di
                sicurezza interpersonale.</p>
            <div name="section1" class="element"><button
                    onclick="showStuff1('answer2', 'text2', this); return false;" id="text2"
                    class="text-primary text-lg lg:text-2xl cursor-pointer focus:outline-none text-left  font-bold my-5 lg:my-5">
                    01.
                    Scegli dove vuoi fare il tuo esame</button>
                <hr class="bg-teal-300  lg:w-105">
                <div class="flex justify-start">
                    <div class="mb-12 rounded-md lg:mb-8 hidden" id="answer2">
                        @include('components.hidden.dropdown')
                    </div>
                </div>
            </div>

            <div name="section2" class="element"><a href="#"
                    onclick="showStuff('answer1', 'text1', this); return false;"
                    class="text-inactive_mobile text-sm lg:text-2xl lg:text-inactive cursor-pointer focus:outline-none text-left  font-bold my-5 lg:my-5"
                    id="text1">02.
                    Scegli il o i tipi di esami da fare</a><br>
                <hr class="bg-teal-300 lg:w-105">
                <form class="hidden" id="answer1">
                    @include('components.hidden.tampono')
                </form>
            </div>

            <div name="section3" class="element"><button
                    class="text-inactive_mobile text-sm lg:text-2xl lg:text-inactive cursor-pointer focus:outline-none text-left  font-bold my-5 lg:my-5">03.
                    Scegli fra le date e orari disponibili</button>
                <div class="hidden" id="answer3">

                    @include('components.hidden.orari_disponibili')
                    @include('components.hidden.orari')
                </div>



                <hr class="bg-teal-300  lg:w-105">
            </div>

            <div name="section4" class="element"><button
                    class="text-inactive_mobile text-sm lg:text-2xl lg:text-inactive cursor-pointer focus:outline-none text-left  font-bold my-5 lg:my-5">04.
                    Dati anagrafici</button>
                <div class="hidden" id="answer4">

                    @include('components.hidden.anagrafici')
                </div>

                <hr class="bg-teal-300 lg:w-105">
            </div>

            <div name="section5" class="element"><button
                    class="text-inactive_mobile text-sm lg:text-2xl lg:text-inactive cursor-pointer focus:outline-none text-left  font-bold my-5 lg:my-5"
                    disabled="">05. Tipo di ricevuta</button>
                <hr class="bg-teal-300 lg:w-105">
            </div>

            <div name="section6" class="element"><button
                    class="text-inactive_mobile text-sm lg:text-2xl lg:text-inactive cursor-pointer focus:outline-none text-left  font-bold my-5 lg:my-5"
                    disabled="">06. Metodo di pagamento</button>
                <hr class="bg-teal-300 lg:w-105">
            </div>
        </div>
    </div>


    @include('components.footer')
</body>

</html>
