<div class="w-full mt-12 px-8 pb-20 justify-center md:px-36 lg:pb-48 xl:container xl:ml-36">
    <h2 class="pt-12 text-2xl font-bold text-center  pb-9 lg:py-16 lg:text-3xl">TAMPONE RAPIDO ANTIGENICO
    </h2>
    <div class="px-4 pt-6 opacity-80 bg-card rounded-2xl lg:p-12 bg-teal-700">
        <p class="mb-6 text-2xl font-bold text-center text-white lg:text-3xl lg:mb-7">Prenota ora i tamponi</p>
        <p class="mb-8 text-base text-center text-white lg:px-20">Ti ricordiamo che per procedere alla prenotazione di un
            Test medico sanitario è necessario essere maggiorenni. Per i minorenni sarà il genitore o tutore a
            effettuare la prenotazione. Clicca il bottone seguente per iniziare la fase di prenotazione.</p>
        <div class="flex flex-col justify-center w-full gap-5 mx-auto md:flex-row lg:mt-20 pb-9">
            <a class="w-full px-6 py-4 text-xl font-bold text-center text-white uppercase rounded-full outline-none bg-active lg:text-xl hover:shadow-md bg-teal-400"
                href="/subscription">Abbonamenti</a>
            <a href="{{ url('reservation') }}"
                class="w-full px-6 py-4 text-xl font-bold text-white uppercase rounded-full text-center outline-none bg-active lg:text-xl hover:shadow-md bg-teal-400">Acquista
                Tampone</a>
        </div>
    </div>
</div>
