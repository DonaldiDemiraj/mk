<div>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <div class="absolute top-0 w-full bg-blue-800">
        <div class="flex justify-center w-full">
            <div class="flex justify-between w-full px-8 sm:px-16 xl:container ">
                <div class="self-center w-24 py-5 sm:w-28 "><a href="/"><img
                            src="https://medical-desk-it.web.app/images/logo.png" alt="logo"></a></div>
                <div class="flex items-center">
                    <div class="relative">
                        <div class="flex items-center w-full h-12">
                            <div
                                class="flex items-center mr-6 h-full bg-white cursor-pointer sm:block rounded-full md:rounded-md text-primary hover:bg-active hover:texy-white transition">
                                <div class="px-8 hidden sm:flex items-center text-lg h-full font-medium">Accedi</div>
                                <div class="w-12 h-12 sm:hidden flex items-center justify-center"><svg
                                        class="w-4 h-5 object-cover" width="14" height="17" viewBox="0 0 14 17"
                                        fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M10.3334 4.33333C10.3334 5.21739 9.98223 6.06523 9.35711 6.69036C8.73198 7.31548 7.88414 7.66667 7.00008 7.66667C6.11603 7.66667 5.26818 7.31548 4.64306 6.69036C4.01794 6.06523 3.66675 5.21739 3.66675 4.33333C3.66675 3.44928 4.01794 2.60143 4.64306 1.97631C5.26818 1.35119 6.11603 1 7.00008 1C7.88414 1 8.73198 1.35119 9.35711 1.97631C9.98223 2.60143 10.3334 3.44928 10.3334 4.33333V4.33333ZM7.00008 10.1667C5.45299 10.1667 3.96925 10.7812 2.87529 11.8752C1.78133 12.9692 1.16675 14.4529 1.16675 16H12.8334C12.8334 14.4529 12.2188 12.9692 11.1249 11.8752C10.0309 10.7812 8.54718 10.1667 7.00008 10.1667V10.1667Z"
                                            stroke="#09425A" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                    </svg></div>
                            </div>
                            <div class="w-18 md:w-44">
                                {{-- @include('components.dropdown') --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
