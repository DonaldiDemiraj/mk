{{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}
<div>
    <button class="flex items-center px-4 py-2 border-2 rounded-full hover:shadow-lg"><svg class="w-6 mr-2"
            version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000"
            enable-background="new 0 0 1000 1000" fill="#0FB7B6">
            <g>
                <path
                    d="M990,503.4c0,25.9-21,46.9-46.9,46.9H56.9c-25.9,0-46.9-21-46.9-46.9v-4.6c0-25.9,21-46.9,46.9-46.9h886.1c25.9,0,46.9,21,46.9,46.9V503.4z">
                </path>
                <path
                    d="M430.9,131.1c18.3,18.3,18.3,48.1,0,66.4L93.1,535.2c-18.3,18.3-48.1,18.3-66.4,0l-2.9-2.9C5.5,514,5.5,484.3,23.9,466l337.7-337.7c18.3-18.3,48.1-18.3,66.4,0L430.9,131.1z">
                </path>
                <path
                    d="M430.9,868.9c18.3-18.3,18.3-48.1,0-66.4L93.1,464.8c-18.3-18.3-48.1-18.3-66.4,0l-2.9,2.9C5.5,486,5.5,515.7,23.9,534l337.7,337.7c18.3,18.3,48.1,18.3,66.4,0L430.9,868.9z">
                </path>
            </g>
        </svg>
        <p class="font-medium focus:outline-none text-inactive_mobile">Indietro</p>
    </button>
    <form class="mt-8 mb-8">
        <div class="p-4 pb-8 mb-8 border-2 rounded-md border-active border-teal-400">
            <h3 class="my-2 text-base font-bold text-primary lg:text-2xl lg:my-6">#1 - Tampone antigenico rapido</h3>
            <div class="grid grid-cols-1 lg:grid-cols-2 lg:gap-x-28 lg:gap-y-6 ">
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="firstname0">Nome *</label>
                    <input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="text" name="person[0].firstname" id="firstname0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="lastname0">Cognome *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="text" name="person[0].lastname" id="lastname0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="email0">Email *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="email" name="person[0].email" id="email0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="confirm_email0">Confirm Email *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="email" name="person[0].confirm_email" id="confirm_email0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="phone0">Cellulare *</label>
                    <div id="phone0">
                        <div class="flex w-full h-auto mt-1.5 react-tel-input">
                            <div class="special-label">Phone</div>
                            <input
                                class="border border-teal-400 border-active rounded-md text-sm text-primary  w-full h-auto py-2 px-3 ml-20  lg:text-base form-control"
                                placeholder="1 (702) 123-4567" type="tel" value="+39">
                        </div>
                    </div>
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="confirm_phone0">Confirm Cellulare *</label>
                    <div id="confirm_phone0">
                        <div class="flex w-full h-auto mt-1.5 react-tel-input">
                            <div class="special-label">Phone</div><input
                                class="border border-teal-400 border-active rounded-md text-sm text-primary  w-full h-auto py-2 px-3 ml-20  lg:text-base form-control"
                                placeholder="1 (702) 123-4567" type="tel" value="+39">
                        </div>
                    </div>
                    <p class="text-red"></p>
                </div>

                <div class="flex flex-col"><label class="text-sm font-medium text-primary mt-3.5 lg:text-base lg:mt-0"
                        for="0">Tipo Documento *</label><select
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary bg-white  py-2 px-3 mt-1.5 lg:text-base"
                        name="person[0].document_type" id="0">
                        <option value="Carta Identita">Carta Identita</option>
                        <option value="Passaporto">Passaporto</option>
                        <option value="Patente Guida">Patente Guida</option>
                    </select>
                    <p class="text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="document_nr0">Nr. Documento *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="text" name="person[0].document_nr" id="document_nr0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col" id="nationality0"><label
                        class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0" for="nationality0">Stato
                        di Residenza *</label>
                    <select name="country"
                        class="form-control border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        required="">
                        <option value="" selected=""></option>
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antarctica">Antarctica</option>
                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Bouvet Island">Bouvet Island</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the
                        </option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                        <option value="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="France Metropolitan">France Metropolitan</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Territories">French Southern Territories</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                        <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of
                        </option>
                        <option value="Korea, Republic of">Korea, Republic of</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Lao, People's Democratic Republic">Lao, People's Democratic Republic</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macau">Macau</option>
                        <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav
                            Republic of</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Namibia">Namibia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau">Palau</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Pitcairn">Pitcairn</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russian Federation">Russian Federation</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                        <option value="Saint Lucia">Saint Lucia</option>
                        <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                        <option value="Samoa">Samoa</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="South Georgia and the South Sandwich Islands">South Georgia and the South
                            Sandwich Islands</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="St. Helena">St. Helena</option>
                        <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                        <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Emirates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States">United States</option>
                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands
                        </option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                        <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                        <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                        <option value="Western Sahara">Western Sahara</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Yugoslavia">Yugoslavia</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                    </select>
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="birthday0">Data di nascita *</label>
                    <div class="flex flex-col" id="birthday0">
                        <input type="date"
                            class="outline-nome border border-teal-400 rounded-md text-sm text-primary  py-2.5 px-3 mt-1.5 lg:text-base w-full"
                            value="">
                    </div>
                    <p class="text-red"></p>
                </div>
                <div class="flex flex-col"><label
                        class="text-sm font-medium text-primary  lg:text-base mt-3.5 lg:mt-0">Genere *</label>
                    <div id="sex0">
                        <div class="flex items-center mt-3">
                            <input class="regular-radio border-teal-300" type="radio" name="person[0].sex" id="male_0"
                                value="M">
                            <label for="male_0"></label>
                            <label for="male_0" class="px-2.5 sm:text-2xl sm:pr-8 text-sm font-medium">M</label>
                            <input class="regular-radio border-teal-300" type="radio" name="person[0].sex" id="female_0"
                                value="F">
                            <label for="female_0"></label><label for="female_0"
                                class="px-2.5 sm:text-2xl text-sm font-medium">F</label>
                        </div>
                    </div>
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="birthplace0">Luogo di nascita *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="text" name="person[0].birthplace" id="birthplace0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="postal_code0">Cap *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="number" name="person[0].postal_code" id="postal_code0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="address0">Indirizzo *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="text" name="person[0].address" id="address0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="city0">Citta *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="text" name="person[0].city" id="city0">
                    <p class=" text-red"></p>
                </div>
                <div class="flex flex-col"><label class="text-sm font-medium text-primary  mt-3.5 lg:text-base lg:mt-0"
                        for="province0">Provincia *</label><input
                        class="border border-teal-400 rounded-md outline-none text-sm text-primary  py-2 px-3 mt-1.5 lg:text-base"
                        type="text" name="person[0].province" id="province0">
                    <p class=" text-red"></p>
                </div><input class="absolute hidden" type="text" name="person[0].servicename" readonly=""
                    value="Tampone antigenico rapido">
            </div>
        </div>
        <div class="flex flex-col mt-4">
            <div class="flex mt-2.5">
                <div><input class="regular-checkbox" type="checkbox" name="checkbox1" id="term1"><label
                        for="term1"></label></div><label class="pl-3 text-sm font-medium text-primary">Dichiaro di aver
                    letto l'Informativa Privacy e, cliccando sul bottone 'Registrati', dichiaro di prendere atto che i
                    miei dati personali <span class="font-bold hover:underline inline">(Vedi di più)</span><span
                        class="hidden">– ivi inclusi i dati relativi alla salute - saranno trattati dalla
                        Società per le finalità di cui al punto 2 a) dell'Informativa medesima (finalità contrattuali e
                        di legge). Qualora soggetto diverso dall'Assicurato e dal beneficiario ove previsto, dichiaro
                        altresì di provvedere a rendere noti i contenuti della presente informativa
                        all'assicurato/beneficiario alla prima occasione di contatto con quest'ultimo/i </span><span
                        class="font-bold hover:underline hidden">(Vedi di meno)</span></label>
            </div>
            <p class=" text-red"></p>
            <div class="flex mt-4 lg:mt-8">
                <div>
                    <input class="regular-checkbox border-teal-400 border-2" type="checkbox" name="checkbox2"
                        id="term2">
                    <label for="term2"></label>
                </div>
                <label class="pl-3 text-sm font-medium text-primary">Dichiaro di aver
                    letto l'Informativa Privacy e, anche ai sensi dell'art. 58 del D.Lgs. 206/2005 e s.m.i (Codice del
                    Consumo) <span class="font-bold hover:underline inline">(Vedi di più)</span>
                    <span class="font-bold hover:underline hidden">(Vedi di meno)</span>
                </label>
            </div>
            <p class=" text-red"></p>
        </div>
        <button
            class=" bg-active bg-teal-400 focus:outline-none uppercase cursor-pointer rounded-full text-white font-bold text-base py-4 mt-11 lg:px-8 hover:shadow-lg w-full md:w-auto"
            type="button" id="open-btn">Vedi Dettagli
        </button>
    </form>


    <div class=" z-10 mt-5 inset-0 overflow-y-0 hidden" aria-labelledby="modal-title" role="dialog" aria-modal="true"
        id="modalcc">
        <div class="flex inset-0 bg-gray-900 bg-opacity-50 transition-opacity" aria-hidden="true"></div>
        {{-- <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span> --}}

        <div class="modal-dialog relative w-auto pointer-events-none">
            <div
                class="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-gray-200 bg-clip-padding rounded-md outline-none text-current">
                <div
                    class="modal-header flex flex-shrink-0 items-center justify-between p-4 border-b border-teal-400 rounded-t-md">
                    <h5 class="text-xl font-medium leading-normal text-gray-800" id="exampleModalLongLabel">
                        #1
                    </h5>
                    <button type="button"
                        class="btn-close box-content w-4 h-4 p-1 text-black border-none rounded-none opacity-50 focus:shadow-none focus:outline-none focus:opacity-100 hover:text-black hover:opacity-75 hover:no-underline"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body relative p-4" style="min-height: 1500px">This is some placeholder content to
                    show the scrolling behavior for modals. Instead of repeating the text the modal, we use an inline
                    style set a minimum height, thereby extending the length of the overall modal and demonstrating the
                    overflow scrolling. When content becomes longer than the height of the viewport, scrolling will move
                    the modal as needed.</div>
                <div class="flex justify-between lg:justify-end ">
                    <button
                        class="flex items-center bg-gray-400 mb-5 font-bold text-base uppercase rounded-full bg-inactive text-gray-text px-8 lg:px-12 py-2.5 hover:shadow-md">Modificare<svg
                            class="ml-3" width="10" height="10" viewBox="0 0 10 10" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M9.44259 2.32051L7.763 0.728506C7.54379 0.533338 7.25654 0.421354 6.9559 0.413855C6.65525 0.406356 6.36219 0.503865 6.13245 0.687835L0.615564 5.91704C0.417425 6.10644 0.294055 6.35467 0.266161 6.62008L0.00257636 9.04295C-0.00568122 9.12805 0.00596884 9.21383 0.036696 9.29417C0.0674231 9.37452 0.116471 9.44745 0.180343 9.50777C0.23762 9.56162 0.305549 9.60422 0.380234 9.63313C0.454919 9.66205 0.534891 9.6767 0.615564 9.67626H0.670733L3.22689 9.45547C3.5069 9.42904 3.76879 9.3121 3.96861 9.12429L9.48549 3.89508C9.69962 3.68067 9.81534 3.39454 9.8073 3.09939C9.79926 2.80425 9.66811 2.52416 9.44259 2.32051V2.32051ZM7.35843 4.26113L5.71562 2.70399L6.91095 1.54194L8.5844 3.12813L7.35843 4.26113Z"
                                fill="#5B5B5B"></path>
                        </svg></button><button
                        class="font-bold text-base uppercase lg:ml-2 bg-teal-400 mb-5 mr-3 rounded-full bg-active text-white px-8 py-2.5 lg:px-14 hover:oppacity-80 hover:shadow-md">AVANTI</button>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    let modall = document.getElementById('modalcc');
    let btn1 = document.getElementById('open-btn');

    btn1.onclick = function() {
        modall.classList.remove('hidden')
    }
</script>
