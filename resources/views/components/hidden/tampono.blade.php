<div class="mb-4 mt-10 text-sm lg:text-base text-blue-900">Puoi prenotare più esami per te stesso o
    anche
    esami diversi per più persone</div><button
    class="flex items-center px-4 py-2 border-2 rounded-full hover:shadow-lg"><svg class="w-6 mr-2" version="1.1"
        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" fill="#0FB7B6">
        <g>
            <path
                d="M990,503.4c0,25.9-21,46.9-46.9,46.9H56.9c-25.9,0-46.9-21-46.9-46.9v-4.6c0-25.9,21-46.9,46.9-46.9h886.1c25.9,0,46.9,21,46.9,46.9V503.4z">
            </path>
            <path
                d="M430.9,131.1c18.3,18.3,18.3,48.1,0,66.4L93.1,535.2c-18.3,18.3-48.1,18.3-66.4,0l-2.9-2.9C5.5,514,5.5,484.3,23.9,466l337.7-337.7c18.3-18.3,48.1-18.3,66.4,0L430.9,131.1z">
            </path>
            <path
                d="M430.9,868.9c18.3-18.3,18.3-48.1,0-66.4L93.1,464.8c-18.3-18.3-48.1-18.3-66.4,0l-2.9,2.9C5.5,486,5.5,515.7,23.9,534l337.7,337.7c18.3,18.3,48.1,18.3,66.4,0L430.9,868.9z">
            </path>
        </g>
    </svg>
    <p class="font-medium focus:outline-none text-inactive_mobile">Indietro</p>
</button>
<div class="mt-8 mb-8">
    <div class="grid grid-cols-1 gap-4 lg:grid-cols-2 xl:grid-cols-3">
        <div>
            <div class="bg-active rounded-md  mb-2 radio-lbl bg-gray-500" id="out1">
                <div class="flex items-center">
                    <p class="pt-5 ml-5 text-sm font-medium text-white">Tampone antigenico rapido
                    </p>
                    <div class="cursor-pointer"><img class="pt-5 ml-2"
                            src="https://medical-desk-it.web.app/images/information-button.png" alt="information"></div>
                </div><input name="service.2.price" readonly="" class="hidden" value="25">
                <div class="flex justify-between pb-6 mx-5 mt-9">
                    <p class="text-3xl font-bold text-white">25€</p>
                    <div class="flex">
                        <p class="pt-1 mr-2 text-lg text-primary">Qta</p>
                        <select onclick="myFunction()" id="go"
                            class="cursor-pointer outline-none text-2xl text-primary bg-transparent rounded border-2 border-primary pl-4 pr-8 py-0.5">
                            <option class="text-black" id="1" value="0">0</option>
                            <option class="text-black" id="2" value="1">1</option>
                            <option class="text-black" id="3" value="2">2</option>
                            <option class="text-black" id="4" value="3">3</option>
                            <option class="text-black" id="5" value="4">4</option>
                            <option class="text-black" id="6" value="5">5</option>
                        </select>
                    </div>
                </div>
            </div>
            <input class="absolute hidden" type="text" name="service.2.serviceName" readonly=""
                value="Tampone antigenico rapido">
        </div>
    </div>
    <div class=""></div>
    <button onclick="showStuff3('answer3', 'text3', this); return false;" id="text3"
        class="bg-active bg-gray-500 focus:outline-none uppercase cursor-pointer rounded-full text-white font-bold text-base py-4 mt-11 lg:px-8 hover:shadow-lg w-full md:w-auto"
        type="submit">AVANTI</button>
</div>
