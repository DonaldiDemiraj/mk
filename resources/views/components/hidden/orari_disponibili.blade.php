<button class="flex items-center px-4 py-2 border-2 rounded-full hover:shadow-lg"><svg class="w-6 mr-2"
        version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000"
        fill="#0FB7B6">
        <g>
            <path
                d="M990,503.4c0,25.9-21,46.9-46.9,46.9H56.9c-25.9,0-46.9-21-46.9-46.9v-4.6c0-25.9,21-46.9,46.9-46.9h886.1c25.9,0,46.9,21,46.9,46.9V503.4z">
            </path>
            <path
                d="M430.9,131.1c18.3,18.3,18.3,48.1,0,66.4L93.1,535.2c-18.3,18.3-48.1,18.3-66.4,0l-2.9-2.9C5.5,514,5.5,484.3,23.9,466l337.7-337.7c18.3-18.3,48.1-18.3,66.4,0L430.9,131.1z">
            </path>
            <path
                d="M430.9,868.9c18.3-18.3,18.3-48.1,0-66.4L93.1,464.8c-18.3-18.3-48.1-18.3-66.4,0l-2.9,2.9C5.5,486,5.5,515.7,23.9,534l337.7,337.7c18.3,18.3,48.1,18.3,66.4,0L430.9,868.9z">
            </path>
        </g>
    </svg>
    <p class="font-medium focus:outline-none text-inactive_mobile">Indietro</p>
</button>
<form onsubmit="sub(event)">
    <div class="mt-8 mb-8">
        <p class="mb-3 text-lg lg:text-2xl lg:mb-2">Data</p>
        <div class="grid grid-cols-2 gap-5 lg:grid-cols-6 xl:grid-cols-8" onclick="fun(event)" id="description">
            <input class="hidden" name="date" type="radio" value="2022-01-24">
            <button type="button"
                class="border-2 border-teal-400 h-12 sm:h-10 rounded-md cursor-pointer radio-lbl hover:shadow-lg"
                id="date0">
                24/01/2022
            </button>
            <input class="hidden" name="date" type="radio" value="2022-01-25">
            <button type="button"
                class="border-2 border-teal-400 h-12 sm:h-10 rounded-md cursor-pointer radio-lbl hover:shadow-lg"
                id="date1">
                26/01/2022
            </button>
            <input class="hidden" name="date" type="radio" value="2022-01-26">
            <button type="button"
                class="border-2 border-teal-400 h-12 sm:h-10 rounded-md cursor-pointer radio-lbl hover:shadow-lg"
                id="date2">
                25/01/2022
            </button>
            <input class="hidden" name="date" type="radio" disabled="" value="2022-01-27">
            <button type="button"
                class="border-2 border-teal-400 h-12 sm:h-10 rounded-md cursor-pointer radio-lbl hover:shadow-lg"
                id="date3">
                27/01/2022
            </button>
            <input class="hidden" name="date" type="radio" value="2022-01-28">
            <button type="button"
                class="border-2 border-teal-400 h-12 sm:h-10 rounded-md cursor-pointer radio-lbl hover:shadow-lg"
                id="date4">
                28/01/2022
            </button>
            <input class="hidden" name="date" type="radio" value="2022-01-29">
            <button type="button"
                class="border-2 border-teal-400 h-12 sm:h-10 rounded-md cursor-pointer radio-lbl hover:shadow-lg"
                id="date5">
                29/01/2022
            </button>
            <input class="hidden" name="date" type="radio" value="2022-01-30">
            <button type="button"
                class="border-2 border-teal-400 h-12 sm:h-10 rounded-md cursor-pointer radio-lbl hover:shadow-lg"
                id="date6">
                30/01/2022
            </button>
            <input class="hidden" name="date" type="radio" value="2022-01-31">
            <button type="button"
                class="border-2 border-teal-400 h-12 sm:h-10 rounded-md cursor-pointer radio-lbl hover:shadow-lg"
                id="date7">
                31/01/2022
            </button>

        </div>
        <p class="text-red"></p>
        <div name="times" class="element"></div>
    </div>
</form>
