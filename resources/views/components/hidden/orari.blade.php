<div name="times" class="element">
    <div class="flex items-center">
        <p class="mt-6 mb-3 text-lg lg:text-2xl lg:mb-2">Orari</p><span class="mt-6 mb-1 ml-4 text-sm">Il valore indicato
            tra parentesi di fianco all'orario rappresenta i posti disponibili</span>
    </div>
    <div class="grid grid-cols-2 gap-5 lg:grid-cols-6 xl:grid-cols-8" onclick="fun(event)" id="description">
        <input class="hidden" name="time" type="radio" id="time0" value="10:30">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time0">
            10:30 <span class="ml-1 text-xs">(4)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time1" value="11:00">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time1">
            11:00 <span class="ml-1 text-xs">(12)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time2" value="11:30"><button
            class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time2">
            11:30 <span class="ml-1 text-xs">(13)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time3" value="12:00">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time3">
            12:00 <span class="ml-1 text-xs">(11)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time4" value="12:30">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time4">
            12:30 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time5" value="13:00">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time5">
            13:00 <span class="ml-1 text-xs">(12)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time6" value="14:30">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time6">
            14:30 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time7" value="15:00">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time7">
            15:00 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time8" value="15:30">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time8">
            15:30 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time9" value="16:00">
        <button class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time9">
            16:00 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time10" value="16:30"><button
            class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time10">
            16:30 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time11" value="17:00"><button
            class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time11">
            17:00 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time12" value="17:30"><button
            class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time12">
            17:30 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time13" value="18:00"><button
            class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time13">
            18:00 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time14" value="18:30"><button
            class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time14">
            18:30 <span class="ml-1 text-xs">(15)</span>
        </button>
        <input class="hidden" name="time" type="radio" id="time15" value="19:00"><button
            class="border-2 rounded-md border-teal-400 h-12 sm:h-10 cursor-pointer radio-lbl hover:shadow-lg"
            for="time15">
            19:00 <span class="ml-1 text-xs">(13)</span>
        </button>


    </div>
    <button onclick="show4('answer4', 'text4', this); return false;" id="text4"
        class="bg-active bg-gray-500 focus:outline-none uppercase cursor-pointer rounded-full text-white font-bold text-base py-4 mt-11 lg:px-8 hover:shadow-lg w-full md:w-auto"
        type="submit">AVANTI</button>
    <p class=" text-red"></p>
</div>
