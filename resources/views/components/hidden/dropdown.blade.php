<div class="relative w-full rounded-md">
    <div class="absolute top-3 lg:top-2 left-3">
        <svg class="w-5 lg:w-6" viewBox="0 0 28 29" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
                <path
                    d="M14.3532 0.00601385C8.66695 -0.192459 3.99731 4.5225 3.99731 10.367C3.99731 16.9998 10.1431 21.8135 13.5878 28.7361C13.7626 29.0876 14.2512 29.0881 14.4267 28.7366C17.5429 22.5076 22.8689 18.3307 23.856 12.3298C24.8779 6.11999 20.4318 0.218264 14.3532 0.00601385ZM14.0067 15.7972C11.1111 15.7972 8.76372 13.3659 8.76372 10.367C8.76372 7.36802 11.1112 4.93672 14.0067 4.93672C16.9023 4.93672 19.2498 7.36802 19.2498 10.367C19.2498 13.3659 16.9023 15.7972 14.0067 15.7972Z"
                    fill="#09425A"></path>
            </g>
            <defs>
                <clipPath id="clip0">
                    <rect width="28" height="29" fill="white"></rect>
                </clipPath>
            </defs>
        </svg>
    </div>
    <select
        class=" form-select appearance-none block w-60 text-right px-3 py-1.5 text-base font-normaltext-gray-700bg-white bg-clip-padding bg-no-repeat border-2 border-solid border-blue-900 rounded transition ease-in-out m-0
    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
        aria-label="Default select example">

        <option selected>Open this select menu</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
    </select>
</div>
